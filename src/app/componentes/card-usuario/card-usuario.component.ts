import { Pessoa } from 'src/app/interfaces/pessoa';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-card-usuario',
  templateUrl: './card-usuario.component.html',
  styleUrls: ['./card-usuario.component.scss']
})
export class CardUsuarioComponent implements OnInit {

  @Input() pessoa: any = {}
  constructor() { }

  ngOnInit(): void {
  }

}
