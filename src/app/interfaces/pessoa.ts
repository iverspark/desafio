export interface Pessoa {
    nome: string;
    cpf: string;  
    id ?: string; 
    email ?: string;
    celular ?: string;
    cidade ?: string;
    foto ?: string;
}