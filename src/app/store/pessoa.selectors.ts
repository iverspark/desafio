import * as fromPessoaReducer from './pessoa.reducer';
import {createFeatureSelector } from '@ngrx/store';

export const pessoaState = createFeatureSelector<fromPessoaReducer.PessoaState>('pessoa');

export const { 
    selectIds,
    selectEntities,
    selectAll,
    selectTotal
} = fromPessoaReducer.pessoaAdapter.getSelectors(pessoaState);