import { Pessoa } from './../interfaces/pessoa';
import { Action } from '@ngrx/store';

export enum PessoaTiposAcao {
    PESSOA_LISTA = '[PESSOA_LISTA] Lista todas pessoas',
    PESSOA_CPF = '[PESSOA_CPF] Lista pessoa com base no CPF',
    PESSOA_NOVO = '[PESSOA_NOVO] Adiciona nova pessoa',
    PESSOA_APAGA = '[PESSOA_APAGA] Apaga uma pessoa'
}

export class PessoaLista implements Action {
    readonly type = PessoaTiposAcao.PESSOA_LISTA;
}

export class PessoaCpf implements Action {
    readonly type = PessoaTiposAcao.PESSOA_CPF;
    constructor(public payload: {cpf: string}) {}
}

export class PessoaNovo implements Action {
    readonly type = PessoaTiposAcao.PESSOA_NOVO;
    constructor(public payload: {pessoa: Pessoa}) {}
}

export class PessoaApaga implements Action {
    readonly type = PessoaTiposAcao.PESSOA_APAGA;
    constructor(public payload: {id: any}) {}
}

export type PessoaAcoes = PessoaLista | PessoaCpf | PessoaNovo | PessoaApaga;
