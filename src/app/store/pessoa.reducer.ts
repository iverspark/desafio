import { Pessoa } from './../interfaces/pessoa';
import * as fromPessoaAcoes from './pessoa.action';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

export interface PessoaState extends EntityState<Pessoa> {}
export const pessoaAdapter: EntityAdapter<Pessoa>  = createEntityAdapter<Pessoa> ();
export const initialState: PessoaState = pessoaAdapter.getInitialState({});

export function reducer(state=initialState, action: fromPessoaAcoes.PessoaAcoes) {
    switch(action.type) {
       
        case fromPessoaAcoes.PessoaTiposAcao.PESSOA_CPF:
            return state;

        case fromPessoaAcoes.PessoaTiposAcao.PESSOA_NOVO:
            return pessoaAdapter.addOne(action.payload.pessoa, state);

        case fromPessoaAcoes.PessoaTiposAcao.PESSOA_APAGA:
            return pessoaAdapter.removeOne(action.payload.id, state);
            
        default:
            return state;
    }
}