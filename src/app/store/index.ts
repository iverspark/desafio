import { ActionReducerMap } from '@ngrx/store';
import { Pessoa } from './../interfaces/pessoa';
import  * as fromPessoaReducer from './pessoa.reducer';

export interface AppState {
    pessoa: fromPessoaReducer.PessoaState;
}

export const appReducers: ActionReducerMap<AppState, any> = {
    pessoa: fromPessoaReducer.reducer
}