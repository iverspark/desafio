import { Observable } from 'rxjs';
import { Component } from '@angular/core';
import { Pessoa } from './interfaces/pessoa';
import { generate, validate  } from 'gerador-validador-cpf';
import { uniqueNamesGenerator, Config, starWars, NumberDictionary  } from 'unique-names-generator';
import { AppState } from './store';
import { Store } from '@ngrx/store';
import * as faker from 'faker';
import { PessoaApaga, PessoaNovo } from './store/pessoa.action';
import * as fromPessoaSelectors from './store/pessoa.selectors';

export enum estadoCpf {
  vazio = 'Por favor insira um CPF!',
  invalido = 'O CPF inserido não é válido!',
  naoEncontrado = 'Não foi encontrado nenhuma pessoa com esse CPF!',
  encontrado = 'Pessoa encontrada!'
}

const customConfig: Config = {
  dictionaries: [starWars],
  separator: '-',
  length: 1,
};

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
 
  public estadosCpf = estadoCpf;
  cpfValido = estadoCpf.vazio;
  pessoa: any = {};
  pessoas$!: Observable<Pessoa[]>;
  cpf = '';
  constructor(private store: Store<AppState>) {

  }

  ngOnInit() {
    for(let i=0; i < 3; i++) {
      this.addNovo();
    }
    this.pessoas$ = this.store.select(fromPessoaSelectors.selectAll);
  }

  addNovo() {
    let pessoa: Pessoa = {
      nome: uniqueNamesGenerator(customConfig),
      cpf: generate({ format: false }),
      id: NumberDictionary.generate({ min: 0, max: 9999 }).toString(),
      email: faker.internet.email(),
      celular: faker.phone.phoneNumber('(##)#####-####'),
      cidade: faker.address.cityName(),
      foto: faker.image.avatar()
    }
    this.store.dispatch(new PessoaNovo({pessoa}));        
  }

  apagarPessoa(p: Pessoa) {
    this.store.dispatch(new PessoaApaga({id: p.id}));
  }

  validarCpf() {
    if (validate(this.cpf)) {
     const igual = this.pessoas$.subscribe( (rs) => {       
       if (rs) {         
        this.cpfValido = estadoCpf.naoEncontrado;
         rs.forEach(p => {                      
          if(p.cpf === this.cpf) {
            this.pessoa = p;
            this.cpfValido = estadoCpf.encontrado;
          } 
         });         
         
       }      
     })          
    } else { 
      this.cpfValido = estadoCpf.invalido;     
    }
  }

  limparCpf() {
    this.cpf = '';
    this.pessoa = {};
    this.cpfValido = estadoCpf.vazio
  }

}
